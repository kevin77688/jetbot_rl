from jetbotSim import Robot, Env
import numpy as np
import cv2
# import matplotlib.pyplot as plt
# from preprocess import preprocess_image, convert_to_state
# from icecream import ic

# ic.configureOutput(includeContext=True)

# Hyperparameters
s_dim = 32 * 32  # State Dimension
a_dim = 3  # Action Dimension


def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.5, 0.5)
    elif action == 1:
        robot.set_motor(0.2, 0.0)
    elif action == 2:
        robot.set_motor(0.0, 0.2)


def execute(obs):
    # Visualize
    global frames
    frames += 1
    img = obs["img"]
    reward = obs["reward"]
    done = obs["done"]

    img = cv2.resize(img, (256, 256))
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    lower_red = np.array([37, 148, 58])
    upper_red = np.array([255, 255, 236])

    lower_black = np.array([0, 0, 0])
    upper_black = np.array([255, 255, 50])

    mask = cv2.inRange(hsv, lower_red, upper_red)
    mask[:140, :] = 0
    mask[150:, :] = 0
    res = cv2.bitwise_and(img, img, mask=mask)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    has_turned = False
    
    mask_black = cv2.inRange(hsv, lower_black, upper_black)
    mask_black[:140, :] = 0
    mask_black[200:, :] = 0
    res_black = cv2.bitwise_and(img, img, mask=mask_black)
    coord_black = cv2.findNonZero(mask_black)
    left_black = np.min(coord_black, axis=0)
    right_black = np.max(coord_black, axis=0)
    if contours:
        largest_contour = max(contours, key=cv2.contourArea)
        x, y, w, h = cv2.boundingRect(largest_contour)
        
        # Draw the bounding box on the original image
        cv2.rectangle(res, (x, y), (x+w, y+h), (0, 0, 255), 2)
        bbox_center_x, bbox_center_y = x + w // 2, y + h // 2
        cv2.circle(res, (bbox_center_x, bbox_center_y), 5, (0, 255, 0), -1)
        if bbox_center_x < 140 and bbox_center_x > 50 and left_black[0][0] >= 0:
            print("Obstacle Right")
            step(1)
            has_turned = True
        elif bbox_center_x > 112 and bbox_center_x < 206 and right_black[0][0] <= 255:
            print("Obstacle Left")
            step(2)
            has_turned = True
        cv2.imshow("res", res)
    if has_turned == False:
        try:
            # Draw the bounding box of red
            cv2.line(res_black, (left_black[0][0], 0), (left_black[0][0], 256), (0, 255, 0), 2)
            cv2.line(res_black, (right_black[0][0], 0), (right_black[0][0], 256), (0, 255, 0), 2)
            if left_black[0][0] > 0:
                print("Road Right")
                step(1)
            elif right_black[0][0] < 255:
                print("Road Left")
                step(2)
            else:
                print("Forward")
                step(0)
        except:
            pass
        cv2.imshow("res_black", res_black)
    cv2.waitKey(1)

    if done:
        frames = 0
        robot.reset()
    print(f"\rframes:{frames}, reward:{reward}, done:{done} ", end="")


frames = 0
robot = Robot()
env = Env()
env.run(execute)
