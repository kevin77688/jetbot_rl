import os
from jetbotSim import Robot, Env
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import random
from collections import deque
from preprocess import preprocess_image, convert_to_state
import wandb

wandb.init(
    project="RL",
    mode="disabled",
)

# Hyperparameters
s_dim = 32 * 32
a_dim = 3

# Global variables for storing state and action between calls
prev_state = None
prev_action = None
prev_reward = 1
current_episode = 1
reward_all = 0
loss_all = []

cumulative_penalty_freq = 0
cumulative_penalty = 0


def customize_reward(reward, state, action):
    global prev_reward
    global cumulative_penalty_freq, cumulative_penalty
    if reward == 0 and prev_reward == 0:
        cumulative_penalty_freq += 1
    else:
        cumulative_penalty_freq = 0

    if reward == 0 and prev_reward == 1:
        prev_reward = reward
        reward = 1
    else:
        prev_reward = reward

    if action == 0:
        reward += 0.5
    else:
        reward -= 0.1
        
    if cumulative_penalty_freq > 5:
        reward -= 0.5 * cumulative_penalty_freq

    # Encourage the robot to more road
    reward += np.sum(state == 0) / state.size

    return reward


def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.5, 0.5)
    elif action == 1:
        robot.set_motor(0.2, 0.0)
    elif action == 2:
        robot.set_motor(0.0, 0.2)


class DQN(nn.Module):
    def __init__(self, input_shape, num_actions):
        super(DQN, self).__init__()
        self.sequnce = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=3, stride=2),  # 1x32x32 -> 32x15x15
            nn.ReLU(),
            nn.Conv2d(32, 64, kernel_size=3, stride=2),  # 32x15x15 -> 64x7x7
            nn.ReLU(),
            nn.Conv2d(64, 64, kernel_size=3, stride=1),  # 64x7x7 -> 64x5x5
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(64 * 5 * 5, 512),
            nn.ReLU(),
            nn.Linear(512, num_actions),
        )

    def forward(self, x):
        x = self.sequnce(x)
        return x


class ReplayBuffer:
    def __init__(self, capacity):
        self.buffer = deque(maxlen=capacity)

    def push(self, state, action, reward, next_state, done):
        self.buffer.append((state, action, reward, next_state, done))

    def sample(self, batch_size):
        state, action, reward, next_state, done = zip(
            *random.sample(self.buffer, batch_size)
        )
        return (
            np.array(state),
            np.array(action),
            np.array(reward),
            np.array(next_state),
            np.array(done),
        )

    def __len__(self):
        return len(self.buffer)


def compute_loss(batch, target_net, policy_net, gamma=0.99):
    states, actions, rewards, next_states, dones = batch
    states = torch.tensor(states, dtype=torch.float32).unsqueeze(1)
    actions = torch.tensor(actions, dtype=torch.long)
    rewards = torch.tensor(rewards, dtype=torch.float32)
    next_states = torch.tensor(next_states, dtype=torch.float32).unsqueeze(1)
    dones = torch.tensor(dones, dtype=torch.float32)

    q_values = policy_net(states).gather(1, actions.unsqueeze(1)).squeeze(1)
    next_q_values = target_net(next_states).max(1)[0]
    expected_q_values = rewards + gamma * next_q_values * (1 - dones)

    loss = (q_values - expected_q_values.detach()).pow(2).mean()
    return loss


# Initialize networks and replay buffer
policy_net = DQN(input_shape=(1, 32, 32), num_actions=a_dim)
if os.path.exists("checkpoint_CNN/policy_net.pth"):
    policy_net.load_state_dict(torch.load("checkpoint_CNN/policy_net.pth"))
target_net = DQN(input_shape=(1, 32, 32), num_actions=a_dim)
target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

optimizer = optim.Adam(policy_net.parameters())
replay_buffer = ReplayBuffer(10000)
batch_size = 32
gamma = 0.0
epsilon_start = 0.0
epsilon_final = 0.0
epsilon_decay = 500


def select_action(state, epsilon):
    if random.random() > epsilon:
        with torch.no_grad():
            state = torch.tensor(state, dtype=torch.float32).unsqueeze(0).unsqueeze(0)
            q_values = policy_net(state)
            return q_values.max(1)[1].item()
    else:
        return random.randrange(a_dim)


def execute(obs):
    global frames, epsilon, prev_state, prev_action
    global current_episode
    global reward_all, loss_all
    global cumulative_penalty_freq, cumulative_penalty
    frames += 1
    img = obs["img"]
    reward = obs["reward"]
    done = obs["done"]
    resized_img = preprocess_image(img)
    state = convert_to_state(resized_img).reshape(32, 32)

    action = select_action(state, epsilon)
    step(action)

    reward = customize_reward(reward, state, prev_action)
    reward_all += reward

    if prev_state is not None and prev_action is not None:
        replay_buffer.push(prev_state, prev_action, reward, state, done)

    if len(replay_buffer) > batch_size:
        batch = replay_buffer.sample(batch_size)
        loss = compute_loss(batch, target_net, policy_net)
        loss_all.append(loss.item())
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if frames % 1000 == 0:
            target_net.load_state_dict(policy_net.state_dict())

    target_frame = (int(current_episode / 50) + 1) * 5000
    if frames > target_frame or reward < -10000.0:
        wandb.log(
            {
                "reward": (reward_all / frames),
                "loss": np.mean(loss_all),
                "frames": frames,
            }
        )
        robot.reset()
        frames = 0
        cumulative_penalty_freq = 0
        cumulative_penalty = 0
        current_episode += 1
        if current_episode % 50 == 0:
            os.makedirs("checkpoint_CNN", exist_ok=True)
            # torch.save(policy_net.state_dict(), "checkpoint_CNN/policy_net.pth")
            # torch.save(
            #     policy_net.state_dict(),
            #     f"checkpoint_CNN/policy_net_{current_episode}.pth",
            # )

    prev_state = state
    prev_action = action

    print(f"\rframes:{frames}, reward:{reward}, done:{done} ", end="")

    epsilon = epsilon_final + (epsilon_start - epsilon_final) * np.exp(
        -1.0 * frames / epsilon_decay
    )


frames = 0
epsilon = epsilon_start
robot = Robot()
env = Env()
env.run(execute)
