import numpy as np
import torch
import torch.nn as nn
from collections import deque
import random

# Weight initialization
def init(module, weight_init, bias_init, gain=1):
    weight_init(module.weight.data, gain=gain)
    bias_init(module.bias.data)
    return module

#Normal distribution module with fixed mean and std.
class FixedNormal(torch.distributions.Normal):
	# Log-probability
	def log_probs(self, actions):
		return super().log_prob(actions).sum(-1)

	# Entropy
	def entropy(self):
		return super().entropy().sum(-1)

	# Mode
	def mode(self):
		return self.mean

#Diagonal Gaussian distribution
class DiagGaussian(nn.Module):
	# Constructor
	def __init__(self, inp_dim, out_dim, std=0.5):
		super(DiagGaussian, self).__init__()

		init_ = lambda m: init(
			m,
			nn.init.orthogonal_,
			lambda x: nn.init.constant_(x, 0)
		)
		self.fc_mean = init_(nn.Linear(inp_dim, out_dim))
		self.std = torch.full((out_dim,), std)

	# Forward
	def forward(self, x):
		mean = self.fc_mean(x)
		return FixedNormal(mean, self.std.to(x.device))

#Policy network
import torch
import torch.nn as nn

class PolicyNet(nn.Module):
	# Constructor
	def __init__(self, s_dim, a_dim, std=0.5):
		super(PolicyNet, self).__init__()

		init_ = lambda m: init(
			m,
			nn.init.orthogonal_,
			lambda x: nn.init.constant_(x, 0),
			nn.init.calculate_gain('relu')
		)		
		#TODO 1: policy network architecture
		self.main = nn.Sequential(
			init_(nn.Linear(s_dim, 128)),
			nn.ReLU(),
			init_(nn.Linear(128, 64)),
			nn.ReLU()
		)
		self.dist = DiagGaussian(64, a_dim, std)

	# Forward
	def forward(self, state, deterministic=False):
		feature = self.main(state)
		dist    = self.dist(feature)

		if deterministic:
			action = dist.mode()
		else:
			action = dist.sample()

		return action, dist.log_probs(action)

	# Output action
	def action_step(self, state, deterministic=True):
		feature = self.main(state)
		dist    = self.dist(feature)

		if deterministic:
			action = dist.mode()
		else:
			action = dist.sample()

		return action

	# Evaluate log-probs & entropy
	def evaluate(self, state, action):
		feature = self.main(state)
		dist    = self.dist(feature)
		return dist.log_probs(action), dist.entropy()

#Value network
class ValueNet(nn.Module):
	# Constructor
	def __init__(self, s_dim):
		super(ValueNet, self).__init__()

		init_ = lambda m: init(
			m,
			nn.init.orthogonal_,
			lambda x: nn.init.constant_(x, 0),
			nn.init.calculate_gain('relu')
		)
		#TODO 2: value network architecture
		self.main = nn.Sequential(
			init_(nn.Linear(s_dim, 128)),
			nn.ReLU(),
			init_(nn.Linear(128, 64)),
			nn.ReLU(),
			init_(nn.Linear(64, 1))
		)


	# Forward
	def forward(self, state):
		return self.main(state)[:, 0]

class ActorCriticCNN(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(ActorCriticCNN, self).__init__()
        self.main = nn.Sequential(
            nn.Conv2d(1, 2, 3, 2),
            nn.ReLU(),
            nn.Conv2d(2, 4, 3, 2),
            nn.ReLU(),
            nn.Conv2d(4, 8, 3, 2),
            nn.ReLU(),
            nn.Conv2d(8, 16, 3, 2),
            nn.ReLU(),
            nn.Flatten(),
            nn.Linear(16 * 13 * 13, 512),
            nn.ReLU(),
            nn.Linear(512, 256),
            nn.ReLU(),
            nn.Linear(256, 64),
            nn.ReLU(),
        )
        self.fc_policy = nn.Linear(64, action_dim)
        self.fc_value = nn.Linear(64, 1)

    def forward(self, x):
        x = self.main(x)
        policy = torch.softmax(self.fc_policy(x), dim=-1)
        value = self.fc_value(x)
        return policy, value
    
class DQN(nn.Module):
    def __init__(self, state_dim, action_dim):
        super(DQN, self).__init__()
        self.sequnce = nn.Sequential(
            nn.Linear(state_dim, 32*32),
			nn.ReLU(),
			nn.Linear(32*32, 32*32),
			nn.ReLU(),
   			nn.Linear(32*32, 32*32),
			nn.ReLU(),
   			nn.Linear(32*32, 32*32),
			nn.ReLU(),
			nn.Linear(32*32, 16*16),
			nn.ReLU(),
			nn.Linear(16*16, 8*8),
			nn.ReLU(),
			nn.Linear(8*8, action_dim)
		)

    def forward(self, x):
        x = self.sequnce(x)
        return x

class ReplayBuffer:
    def __init__(self, capacity):
        self.buffer = deque(maxlen=capacity)

    def push(self, state, action, reward, next_state, done):
        self.buffer.append((state, action, reward, next_state, done))

    def sample(self, batch_size):
        state, action, reward, next_state, done = zip(*random.sample(self.buffer, batch_size))
        return np.array(state), np.array(action), np.array(reward), np.array(next_state), np.array(done)

    def __len__(self):
        return len(self.buffer)
