import cv2
import numpy as np

state_colors = [
    [25, 25, 25],  # Black = Road
    [105, 28, 30],  # Red = Obstacle
    [187, 209, 239],  # Blue = Background
]
np_state_colors = np.array(state_colors)

def preprocess_image(img, is_real=False):
    resize_img = cv2.resize(img, (224, 224))
    resize_img = cv2.cvtColor(resize_img, cv2.COLOR_BGR2RGB)
    if is_real:
        pass
        # resize_img = resize_img / 255
    return resize_img

def convert_to_state(img):
    # Convert each pixel to nearest state color
    img = cv2.resize(img, (32, 32))
    img_reshaped = img.reshape(-1, 3)
    distances = np.sum((img_reshaped[:, np.newaxis, :] - np_state_colors[np.newaxis, :, :]) ** 2, axis=2)
    state = np.argmin(distances, axis=1)
    return state.reshape(32, 32)
