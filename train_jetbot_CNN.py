import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
import sys
import cv2
from jetbotSim import Robot, Env
from preprocess import preprocess_image, convert_to_state
from model import ActorCriticCNN
from icecream import ic, install
ic.configureOutput(includeContext=True)
install()

# Global variables
frames = 0
gamma = 0.99
curr_episode = 0
cumulative_penalty = 0
previous_state = None
previous_action = None
previous_reward = None
previous_done = None

# Hyperparameters
s_dim = 32 * 32             # State Dimension
a_dim = 3                   # Action Dimension
device = 'cuda' if torch.cuda.is_available() else 'cpu'


# Initialize the model, optimizer, and loss function
model = ActorCriticCNN(s_dim, a_dim).to(device)
optimizer = optim.Adam(model.parameters(), lr=1e-3)
mse_loss = nn.MSELoss()


# Step function
def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.5, 0.5)
    elif action == 1:
        robot.set_motor(0.2, 0.)
    elif action == 2:
        robot.set_motor(0., 0.2)

# Execute function
def execute(obs):
    global frames, model, optimizer, curr_episode, cumulative_penalty
    global previous_state, previous_action, previous_reward, previous_done

    img = obs["img"]
    resized_img = preprocess_image(img)
    state = convert_to_state(resized_img)
    state = torch.tensor(state, dtype=torch.float32).flatten().to(device)

    # If this is not the first step, perform the training step
    if previous_state is not None:
        # Get next state
        next_state = state

        # Calculate target value
        with torch.no_grad():
            _, next_value = model(next_state)
            target_value = previous_reward + gamma * next_value * (1 - previous_done)

        # Calculate loss
        policy, value = model(previous_state)
        value_loss = mse_loss(value, target_value)
        advantage = target_value - value
        policy_loss = -torch.log(policy[previous_action]) * advantage
        loss = value_loss + policy_loss

        # Backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        print(f'\rEpisode:{curr_episode}, frames:{frames}, reward:{previous_reward:.3f}, done:{previous_done}, loss:{loss.item():.3f}', end="")

    # Select action for the current state
    policy, value = model(state)
    action = torch.multinomial(policy, 1).item()

    # Perform action
    step(action)

    # Get reward and done flag from observation
    reward = obs['reward']
    done = obs['done']

    if action == 0:
        reward += 1
    if reward < 1:
        cumulative_penalty += 0.5
    else:
        cumulative_penalty = 0
        
    reward -= cumulative_penalty

    # Update global variables for the next step
    previous_state = state
    previous_action = action
    previous_reward = reward
    previous_done = done

    frames += 1
    target_frame = (int(curr_episode / 10) + 1) * 100
    if frames >= target_frame:
        frames = 0
        robot.reset()
        curr_episode += 1
        print('\n')
        if curr_episode % 50 == 0:
            torch.save(model.state_dict(), f'./save/model_{curr_episode}.pth')
            optimizer.param_groups[0]['lr'] = 1e-3

# Initialize the environment and start the simulation
robot = Robot()
env = Env()
env.run(execute)