from jetbotSim import Robot, Env
import numpy as np
import os
import cv2
import torch
import matplotlib.pyplot as plt
from preprocess import preprocess_image, convert_to_state
from model import PolicyNet, ValueNet
from icecream import ic, install
ic.configureOutput(includeContext=True)
install()

# Global Variable
frames = 0
total_episodes = 0
cumulative_penalty = 1
robot = Robot()
env = Env()
buffer = []
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# Hyperparameters
s_dim = 224 * 224           # State Dimension
a_dim = 3                   # Action Dimension
policy_net = PolicyNet(s_dim, a_dim)
value_net = ValueNet(s_dim)
policy_net.to(device)
value_net.to(device)
optimizer_policy = torch.optim.AdamW(policy_net.parameters(), lr=1e-4)
optimizer_value = torch.optim.AdamW(value_net.parameters(), lr=1e-4)
mse_loss = torch.nn.MSELoss()

if os.path.exists('save/policy_net.pth'):
    policy_net.load_state_dict(torch.load('save/policy_net.pth'))
    print('PolicyNet loaded')
    
if os.path.exists('save/value_net.pth'):
    value_net.load_state_dict(torch.load('save/value_net.pth'))
    print('ValueNet loaded')


def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.5, 0.5)
    elif action == 1:
        robot.set_motor(0.2, 0.)
    elif action == 2:
        robot.set_motor(0., 0.2)

def execute(obs):
    global frames, buffer, cumulative_penalty, total_episodes
    frames += 1
    img = obs["img"]
    resized_img = preprocess_image(img)
    state = convert_to_state(resized_img)

    # Convert state to tensor
    state_tensor = torch.FloatTensor(state)
    state_tensor = state_tensor.to(device)
    state_tensor = state_tensor.unsqueeze(0)
    state_tensor = state_tensor.unsqueeze(0)
    # state_tensor = torch.FloatTensor(state).unsqueeze(0)
    # state_tensor = state_tensor.to(device)

    # Select action using PolicyNet
    action, log_prob = policy_net.action_step(state_tensor)
    action = action.item()

    step(action)

    reward = obs['reward']
    done = obs['done']
    
    if not done and reward == 0:
        cumulative_penalty -= 1
    else:
        cumulative_penalty = 0
        
    if not done and action == 0:
        reward += 30
    else:
        reward -= 10
        
    reward += cumulative_penalty

    buffer.append((state, action, reward, log_prob, done))

    frame_target = (int(total_episodes / 50) + 1) * 100
    if frames % frame_target == 0 or done:
        train()
        buffer = []
        frames = 0
        robot.reset()
        total_episodes += 1

    print(f'\rEpisode: {total_episodes}, frames:{frames}, reward:{reward}, done:{done} ', end="")


def train():
    global policy_net, optimizer_policy, buffer
    states, actions, rewards, log_probs, dones = zip(*buffer)

    states = torch.FloatTensor(np.array(states))
    actions = torch.LongTensor(actions)
    rewards = torch.FloatTensor(rewards)
    log_probs = torch.stack(log_probs)
    
    states = states.to(device)
    actions = actions.to(device)
    rewards = rewards.to(device)
    log_probs = log_probs.to(device)

    # Compute value targets
    returns = compute_returns(rewards)
    returns = torch.FloatTensor(returns)
    returns = returns.to(device)

    # Normalize advantages
    advantages = returns
    # advantages /= (advantages.std() + 1e-8)

    # Update PolicyNet
    optimizer_policy.zero_grad()
    ic(log_probs)
    ic(advantages)
    policy_loss = -torch.mean(log_probs * -advantages)
    ic(policy_loss.item())
    policy_loss.backward()
    optimizer_policy.step()

    # Save policy_net
    torch.save(policy_net.state_dict(), 'save/policy_net.pth')

def compute_returns(rewards, gamma=0.98):
    returns = []
    R = 0
    for r in reversed(rewards):
        R = r + gamma * R
        returns.insert(0, R)
    return returns


env.run(execute)
