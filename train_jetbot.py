from jetbotSim import Robot, Env
import torch
import torch.nn as nn
import torch.optim as optim
import random
import numpy as np
import cv2
import os
from icecream import ic
ic.configureOutput(includeContext=True)

import wandb
wandb.init(
    project="RL",
    mode="online",)

from model import DQN, ReplayBuffer
from preprocess import convert_to_state, preprocess_image

# Hyperparameters
state_dim = 32 * 32
action_dim = 3
lr = 1e-3
gamma = 0.8
epsilon_start = 0.9
epsilon_end = 0.1
epsilon_decay = 5000
batch_size = 64
buffer_capacity = 1000
target_update_frequency = 50
max_frames = 50000000
curr_episode = 1
policy_net_path = "checkpoint/policy/"
target_net_path = "checkpoint/target/"
reward_freq = 0
penalty_freq = 0
rotation_freq = 0
cumulative_penalty = 0
cumulative_reward = 0
cumulative_rotation_penalty = 0
loss_all = []

# Initialize components
os.makedirs(policy_net_path, exist_ok=True)
os.makedirs(target_net_path, exist_ok=True)

policy_net = DQN(state_dim, action_dim)
target_net = DQN(state_dim, action_dim)
target_net.load_state_dict(policy_net.state_dict())
target_net.eval()

if os.path.exists(f"{policy_net_path}policy.pth"):
    print("Loading model")
    policy_net.load_state_dict(torch.load(f"{policy_net_path}policy.pth"))
    target_net.load_state_dict(torch.load(f"{target_net_path}target.pth"))

optimizer = optim.Adam(policy_net.parameters(), lr=lr)
replay_buffer = ReplayBuffer(buffer_capacity)
epsilon = epsilon_start


def calculate_reward(reward, action, state):
    global penalty_freq, cumulative_penalty
    
    new_reward = reward
    # Check the central pixel of the image
    central_pixel = state[16, 16]
    if central_pixel == 0:
        new_reward += 1.0
    elif central_pixel == 1 or central_pixel == 2:
        new_reward -= 0.5
    else:
        print(central_pixel)
        new_reward -= 10000.0

    # Encourage the robot to stay on the road
    road_percentage = np.count_nonzero(state == 0) / state.size
    new_reward += road_percentage * 1.0
    
    # Penalize the robot for going off the road
    if road_percentage < 0.2:
        new_reward -= 0.5

    # Encourage the robot to move forward
    if action == 0:
        new_reward += 0.8
    else:
        new_reward -= 0.1

    # Penalize the robot if reward is less than 0.5
    if reward < 1.0:
        penalty_freq += 1
    else:
        penalty_freq = 0
    
    if penalty_freq > 10:
        new_reward -= penalty_freq * 5.0
    
    
    # if action == 0:
    #     new_reward += 0.8
    #     rotation_freq -= 1
    # else:
    #     rotation_freq += 2
    #     new_reward -= 0.05 * rotation_freq
    
    # if new_reward > 0.9:
    #     penalty_freq = 0
    #     cumulative_penalty = 0
    # else:
    #     penalty_freq += 1

    # if penalty_freq > 5:
    #     cumulative_penalty += 1.5
    #     new_reward -= cumulative_penalty

    # new_reward += road_percentage * 1.5
    # new_reward -= 0.01 # Time penalty

    return new_reward


def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.8, 0.8)
    elif action == 1:
        robot.set_motor(0.5, 0.4)
    elif action == 2:
        robot.set_motor(0.4, 0.5)


def execute(obs):
    # Visualize
    global frames, epsilon, robot, max_frames, curr_episode, penalty_freq, cumulative_penalty, cumulative_reward, reward_freq
    global rotation_freq, cumulative_rotation_penalty, loss_all
    frames += 1
    img = obs["img"]
    reward = obs["reward"]
    done = obs["done"]
    resized_img = preprocess_image(img)
    state = convert_to_state(resized_img)

    scaled_state = (state * 127).astype(np.uint8)
    resized_array = cv2.resize(
        scaled_state, (256, 256), interpolation=cv2.INTER_NEAREST
    ).astype(np.uint8)
    cv2.imshow("State", resized_array)
    cv2.waitKey(1)

    original_state = state.copy()
    state = state.flatten()
    action = select_action(state, epsilon)
    step(action)
    # road_percentage = np.count_nonzero(state == 0) / state.size
    next_obs = env.buffer  # Use the buffer directly as next_obs is not available

    reward = calculate_reward(reward, action, original_state)

    nparr = np.fromstring(next_obs[5:], np.uint8)
    next_img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
    resized_next_img = preprocess_image(next_img)
    next_state = convert_to_state(resized_next_img).flatten()

    replay_buffer.push(state, action, reward, next_state, done)

    if len(replay_buffer) > batch_size:
        batch = replay_buffer.sample(batch_size)
        loss = compute_loss(batch)
        loss_all.append(loss.item())
        print(f"Loss: {loss.item()} \t", end="\r")

        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

    if frames % target_update_frequency == 0:
        target_net.load_state_dict(policy_net.state_dict())

    epsilon = max(epsilon_end, epsilon_start - curr_episode / epsilon_decay)

    if frames > max_frames or reward < -50.0 or done:
        # Save the model
        if curr_episode % 50 == 0:
            torch.save(policy_net.state_dict(), f"{policy_net_path}{curr_episode}.pth")
            torch.save(policy_net.state_dict(), f"{policy_net_path}policy.pth")
            torch.save(target_net.state_dict(), f"{target_net_path}{curr_episode}.pth")
            torch.save(target_net.state_dict(), f"{target_net_path}target.pth")
            
        wandb.log({"frames": frames, "loss": np.mean(loss_all)})
        curr_episode += 1
        if curr_episode % 50 == 0:
            max_frames += 500
        robot.reset()
        frames = 0
        reward_freq = 0
        penalty_freq = 0
        rotation_freq = 0
        cumulative_penalty = 0
        cumulative_reward = 0
        cumulative_rotation_penalty = 0
        loss_all = []
    
    print(
        f"\r episode: {curr_episode:04d}, frames: {frames:06d}, reward: {reward:4.2f}, done: {done} ",
        end="",
    )
    frames += 1

    # if done:
    #     robot.reset()
    #     frames = 0


def select_action(state, epsilon):
    if random.random() > epsilon:
        with torch.no_grad():
            state = torch.tensor(state, dtype=torch.float32).unsqueeze(0)
            q_values = policy_net(state)
            return q_values.max(1)[1].item()
    else:
        return random.randrange(action_dim)


def compute_loss(batch):
    states, actions, rewards, next_states, dones = batch

    states = torch.tensor(states, dtype=torch.float32)
    actions = torch.tensor(actions, dtype=torch.int64).unsqueeze(1)
    rewards = torch.tensor(rewards, dtype=torch.float32).unsqueeze(1)
    next_states = torch.tensor(next_states, dtype=torch.float32)
    dones = torch.tensor(dones, dtype=torch.float32).unsqueeze(1)

    current_q_values = policy_net(states).gather(1, actions)
    next_q_values = target_net(next_states).max(1)[0].unsqueeze(1)
    expected_q_values = rewards + (gamma * next_q_values * (1 - dones))

    loss = nn.functional.mse_loss(current_q_values, expected_q_values)
    return loss


frames = 0
robot = Robot()
env = Env()
env.run(execute)
