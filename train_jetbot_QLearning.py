from jetbotSim import Robot, Env
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt
from preprocess import preprocess_image, convert_to_state
from icecream import ic
import hashlib

ic.configureOutput(includeContext=True)

# Global variables
prev_state = None
prev_action = None
prev_reward = 1
frames = 0
robot = Robot()
env = Env()
current_episode = 0

# Hyperparameters
s_dim = 3 ** (8 * 8)  # State Dimension based on 3 values per cell in a 8x8 grid
a_dim = 3  # Action Dimension
alpha = 0.2  # Learning rate
gamma = 0.9  # Discount factor
epsilon = 0.2  # Exploration rate

# Initialize Q-table with a feasible size, for example, a subset
q_table = {}

# Load the latest Q-table if it exists
if os.path.exists("checkpoint/q_table_latest.npy"):
    q_table = np.load("checkpoint/q_table_latest.npy", allow_pickle=True).item()
    
def custom_reward(reward, state):
    global prev_reward
    if reward == 0 and prev_reward == 1:
        prev_reward = reward
        reward = 1
    else:
        prev_reward = reward
        
    # Encourage the robot to more road
    reward += np.sum(state == 0) / state.size
    
    central_pixel = state[4:8, 3:4]
    central_pixel = central_pixel.flatten()
    mask = (central_pixel == 1)
    central_pixel = np.where(mask, central_pixel, 0).astype(float)
    central_pixel *= [0.5, 1, 2, 4]
    penalty = np.sum(central_pixel)
    reward -= penalty
    
    return reward

def encode_state(state):
    """Encode a 32x32 state using a hash function."""
    state_bytes = state.tobytes()
    state_hash = hashlib.md5(state_bytes).hexdigest()
    return state_hash


def choose_action(state_hash):
    """
    Choose an action based on the epsilon-greedy policy
    """
    if np.random.uniform() < epsilon:
        # Explore
        action = np.random.randint(a_dim)
    else:
        # Exploit
        if state_hash not in q_table:
            q_table[state_hash] = np.zeros(a_dim)
        action = np.argmax(q_table[state_hash])
    return action


def step(action):
    global robot
    if action == 0:
        robot.set_motor(0.5, 0.5)
    elif action == 1:
        robot.set_motor(0.2, 0.0)
    elif action == 2:
        robot.set_motor(0.0, 0.2)


def show_state(state):
    scaled_state = (state * 127).astype(np.uint8)
    resized_array = cv2.resize(
        scaled_state, (256, 256), interpolation=cv2.INTER_NEAREST
    ).astype(np.uint8)
    cv2.imshow("State", resized_array)
    cv2.waitKey(1)


def execute(obs):
    global frames
    global prev_state, prev_action
    global prev_reward
    global current_episode

    # Visualize
    frames += 1
    img = obs["img"]
    reward = obs["reward"]
    done = obs["done"]
    resized_img = preprocess_image(img)
    state = convert_to_state(resized_img)
    state_hash = encode_state(state)
    show_state(state)

    reward = custom_reward(reward, state)

    if prev_state is None:
        # Choose a random action for the initial state
        action = np.random.randint(a_dim)
    else:
        # Q-Learning update
        if prev_state not in q_table:
            q_table[prev_state] = np.zeros(a_dim)
        if state_hash not in q_table:
            q_table[state_hash] = np.zeros(a_dim)
        prev_q_value = q_table[prev_state][prev_action]
        max_q_value = np.max(q_table[state_hash])
        q_table[prev_state][prev_action] += alpha * (
            reward + gamma * max_q_value - prev_q_value
        )

        # Choose action using Q-table
        action = choose_action(state_hash)

    # Step the environment
    prev_state = state_hash
    prev_action = action
    step(action)

    target_frame = (current_episode / 50 + 1) * 50
    if frames > target_frame:
        robot.reset()
        frames = 0
        prev_state = None
        prev_action = None
        current_episode += 1
        if current_episode % 10 == 0:
            # Save the Q-table
            os.makedirs("checkpoint", exist_ok=True)
            np.save(f"checkpoint/q_table_{current_episode}.npy", q_table)
            np.save(f"checkpoint/q_table_latest.npy", q_table)
    print(
        f"\rEpisode: {current_episode:4d}, frames:{frames:4d}, reward:{reward:2.1f}, done:{done} ",
        end="",
    )


env.run(execute)
